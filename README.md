# UNEJMTL Minetest Game

The game used in MinetestLab.  

## Installation

- Unzip the archive, rename the folder to `mtl` and
place it in .. minetest/games/

- GNU/Linux: If you use a system-wide installation place
    it in ~/.minetest/games/.

The Minetest engine can be found at [GitHub](https://github.com/minetest/minetest).

For further information or help, see:  
https://wiki.minetest.net/Installing_Mods

## Licensing

See `LICENSE.txt`

## List of mods

| NOM                    | SOURCE                                                                             | FORK                                                                   |
|------------------------|------------------------------------------------------------------------------------|------------------------------------------------------------------------|
| areas                  | https://github.com/minetest-mods/areas |    |
| default                | https://github.com/minetest-game-mods/default |    |
| server_news            | https://github.com/Ezhh/server_news |    |
| mapserver_mod          | https://github.com/minetest-mapserver/mapserver_mod |    |
| maptools               | https://github.com/minetest-mods/maptools |    |
| monitoring             | https://github.com/minetest-monitoring/monitoring |    |
| playerfactions         | https://github.com/mt-mods/playerfactions  |    |
| unej                   | https://gitlab.com/iri-research-org/urbanum/minetest/minetest-mod-unej |    |
| we_undo                | https://github.com/HybridDog/we_undo |    |
| whitelist              | https://github.com/AntumMT/mod-whitelist.git |    |
| worldedit              | https://github.com/Uberi/Minetest-WorldEdit |    |
