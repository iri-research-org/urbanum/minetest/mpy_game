ARG PUID="1000"
ARG GUID="1000"
ARG BUILD_DATE
ARG VERSION
ARG MINETEST_RELEASE


FROM alpine

# ENV MINETEST_GAME_VERSION master
ENV MINETEST_RELEASE_DEFAULT "5.8.0"
ENV UNEJ_GAME_RELEASE "master"
ENV LDFLAGS "-lintl"
ENV IRRLICHT_VERSION "1.9.0mt10"
ENV SPATIALINDEX_VERSION "1.9.3"
ENV LUAJIT_VERSION "2.1"
ENV LUAROCKS_VERSION "3.11.0"

WORKDIR /usr/src/minetest

RUN apk add --no-cache \
	  bash \
	  grep \
	  git \
	  jq \
	  build-base \
	  cmake \
	  bzip2-dev \
	  libpng-dev \
	  jpeg-dev \
	  libxxf86vm-dev \
	  mesa-dev \
	  sqlite-dev \
	  libogg-dev \
	  libintl \
	  gettext-dev \
	  gettext \
	  libvorbis-dev \
	  openal-soft-dev \
	  curl \
	  curl-dev \
	  freetype-dev \
	  zlib-dev \
	  zstd-dev \
	  gmp-dev \
	  jsoncpp-dev \
	  postgresql-dev \
	  ca-certificates \
	  doxygen \
	  ninja \
	  hiredis-dev \
	  icu-dev \
	  leveldb-dev \
	  openssl-dev \
	  libtool \
	  libxi-dev \
	  ncurses-dev \
	  python3-dev \
	  wget \
	  tar

RUN	git config --global advice.detachedHead false && \
	git clone --depth=1 -b ${MINETEST_GAME_VERSION:-${MINETEST_RELEASE:-${MINETEST_RELEASE_DEFAULT}}} https://github.com/minetest/minetest_game.git ./games/minetest_game && \
	rm -fr ./games/minetest_game/.git

COPY startserver.sh /usr/local/sbin/startserver.sh

WORKDIR /usr/src/
RUN env
RUN git clone --recursive https://github.com/jupp0r/prometheus-cpp/ && \
		cd prometheus-cpp && \
		cmake -B build \
			-DCMAKE_INSTALL_PREFIX=/usr/local \
			-DCMAKE_BUILD_TYPE=Release \
			-DENABLE_TESTING=0 \
			-GNinja && \
		cmake --build build && \
		cmake --install build && \
	cd /usr/src/ && \
	git clone --recursive https://github.com/libspatialindex/libspatialindex -b ${SPATIALINDEX_VERSION} && \
		cd libspatialindex && \
		cmake -B build \
			-DCMAKE_INSTALL_PREFIX=/usr/local && \
		cmake --build build && \
		cmake --install build && \
	cd /usr/src/ && \
	git clone --recursive https://luajit.org/git/luajit.git -b "v${LUAJIT_VERSION}" && \
		cd luajit && \
		make && make install && \
	cd /usr/src/ && \
	mkdir luarocks && cd luarocks && \
	wget -O- "https://luarocks.org/releases/luarocks-${LUAROCKS_VERSION}.tar.gz" | tar zxpf - --strip-components=1 && \
		./configure --with-lua-include=/usr/local/include/luajit-${LUAJIT_VERSION} && make && make install && \
		luarocks install luasocket && \
		luarocks install lua-cjson && \
		luarocks install luasec && \
	cd /usr/src/ && \
	git clone --depth=1 https://github.com/minetest/irrlicht/ -b ${IRRLICHT_VERSION} && \
		cp -r irrlicht/include /usr/include/irrlichtmt

WORKDIR /usr/src/minetest

RUN if [ -z ${MINETEST_RELEASE+x} ]; then \
		MINETEST_RELEASE=$(curl -sX GET "https://api.github.com/repos/minetest/minetest/releases/latest" \
		| awk '/tag_name/{print $4;exit}' FS='[""]'); \
	fi && \
	curl -s -o \
		/tmp/minetest-src.tar.gz -L \
		"https://github.com/minetest/minetest/archive/${MINETEST_RELEASE:-${MINETEST_RELEASE_DEFAULT}}.tar.gz" && \
	tar xf \
		/tmp/minetest-src.tar.gz --strip-components=1 && \
	cmake -B build \
		-DCMAKE_INSTALL_PREFIX=/usr/local \
		-DCMAKE_BUILD_TYPE=Release \
		-DBUILD_SERVER=TRUE \
		-DENABLE_PROMETHEUS=TRUE \
		-DBUILD_UNITTESTS=FALSE \
		-DBUILD_CLIENT=FALSE \
		-DENABLE_CURL=TRUE \
		-DENABLE_FREETYPE=TRUE \
		-DENABLE_GETTEXT=TRUE \
		-DENABLE_LUAJIT=TRUE \
		-DENABLE_REDIS=TRUE \
		-DENABLE_POSTGRESQL=TRUE \
		-DENABLE_LEVELDB=TRUE \
		-DENABLE_SYSTEM_GMP=TRUE \
		-DENABLE_SOUND=FALSE \
		-DRUN_IN_PLACE=FALSE \
		-DVERSION_EXTRA="${VERSION}_${BUILD_DATE}" \
		-DGETTEXT_LIBRARY=-lintl \
		-GNinja && \
	cmake --build build && \
	cmake --install build && \
	cp -r build/locale /usr/local/share/minetest/ && \
	curl -s -o \
		/tmp/minetest_game.tar.gz -L \
		"https://github.com/minetest/minetest_game/archive/${MINETEST_RELEASE:-${MINETEST_RELEASE_DEFAULT}}.tar.gz" && \
	mkdir -p /usr/local/share/minetest/games/minetest_game && \
	tar xf /tmp/minetest_game.tar.gz -C /usr/local/share/minetest/games/minetest_game --strip-components=1 && \
    mkdir -p /usr/local/share/minetest/games/mpy

COPY game.conf minetest.conf settingtypes.txt /usr/local/share/minetest/games/mpy/
COPY mods /usr/local/share/minetest/games/mpy/mods

FROM alpine

ARG PUID
ARG GUID
ARG BUILD_DATE
ARG VERSION

LABEL build_version="iri-research.org version:- ${VERSION} Build-date:- ${BUILD_DATE}"
LABEL maintainer="I.R.I."


RUN apk add --no-cache sqlite-libs curl gmp libstdc++ libgcc libpq libintl jsoncpp zstd-libs bash \
		gettext rsync hiredis lua-socket sqlite zstd jq leveldb && \
		
	addgroup -g ${GUID} minetest && \
	adduser -D -u ${PUID} -G minetest -h /var/lib/minetest minetest && \
	chown -R minetest:minetest /var/lib/minetest && \
	mkdir /var/log/minetest && \
	mkdir -p /usr/local/share/doc && \
	chown -R minetest:minetest /var/log/minetest

WORKDIR /var/lib/minetest

COPY --from=0 /usr/local/share/minetest /usr/local/share/minetest
COPY --from=0 /usr/local/bin /usr/local/bin
COPY --from=0 /usr/local/lib /usr/local/lib
COPY --from=0 /usr/local/share /usr/local/share
COPY --from=0 /usr/local/share/doc/minetest /usr/local/share/doc/minetest
COPY --from=0 --chown=minetest:minetest --chmod=755 /usr/local/sbin/startserver.sh /usr/local/sbin/startserver.sh

USER minetest:minetest

EXPOSE 30000/udp 30000/tcp 29999
VOLUME /etc/minetest
VOLUME /var/lib/minetest/worlds
VOLUME /var/log/minetest

CMD ["/usr/local/sbin/startserver.sh"]